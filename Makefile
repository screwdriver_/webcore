NAME = webcore.a
CC = g++
CFLAGS = -Wall -Wextra -Werror -Wno-unused-result -std=c++17 -g3

SRC_DIR = src/
OBJ_DIR = obj/
DIRS := $(shell find $(SRC_DIR) -type d)
SRCS := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDRS := $(shell find $(SRC_DIR) -type f -name "*.hpp")
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIRS))
OBJS := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRCS))

all: $(NAME) tags

$(NAME): $(OBJ_DIRS) $(OBJS)
	ar rc $(NAME) $(OBJS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDRS)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIRS): $(SRC) $(HDR)
	mkdir -p $(OBJ_DIRS)

tags: $(SRC) $(HDR)
	ctags -R $(SRC_DIR)/* --languages=c,c++

clean:
	rm -rf $(OBJ_DIRS)

fclean: clean
	rm -f $(NAME)
	rm -f tags

re: fclean all

.PHONY: all clean fclean re
