#include "web_core.hpp"

using namespace Webcore;

std::string Webcore::read_file(const std::string& path)
{
	std::ifstream stream(path);

	stream.seekg(0, std::ios::end);
	const auto length = stream.tellg();
	stream.seekg(0);
	std::string buffer;
	buffer.resize(length);
	stream.read(&buffer[0], length);
	return buffer;
}

void Webcore::set_tags(std::string &temp,
	const std::string &tag, const std::string &value)
{
	const std::string s(std::string("$(") + tag + ')');
	size_t pos = temp.find(s);

	while(pos != std::string::npos)
	{
		temp.replace(pos, s.size(), value);
		pos = temp.find(s, pos + value.size());
	}
}
