#include "web_core.hpp"

using namespace Webcore;

const std::string method_texts[] = {
	"GET",
	"HEAD",
	"POST",
	"PUT",
	"DELETE",
	"TRACE",
	"OPTIONS",
	"CONNECT",
	"PATCH"
};

http_method Webcore::get_method(const std::string& method)
{
	for(size_t i = 0; i < sizeof(method_texts) / sizeof(std::string); ++i)
		if(method_texts[i] == method)
			return static_cast<http_method>(i);
	throw std::runtime_error("Unknown method!");
}
