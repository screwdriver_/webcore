#include "web_core.hpp"

using namespace Webcore;

Call::Call(Instance *instance, const int sock)
	: instance{instance}, sock{sock}
{
	read_request();
	instance->log(std::string("New client from `") + get_address()
		+ "`, Request URI: " + get_URI());
}

const std::string &Call::get_address() const
{
	const auto &address = get_request_field("REMOTE_ADDR");

	if(address)
		return *address;
	throw std::runtime_error("Failed to retrieve client address!");
}

http_method Call::get_request_method() const
{
	const auto method = get_request_field("REQUEST_METHOD");

	if(method)
		return get_method(*method);
	throw std::runtime_error("Failed to retrieve request method!");
}

const std::string &Call::get_URI() const
{
	const auto uri = get_request_field("REQUEST_URI");

	if(uri)
		return *uri;
	throw std::runtime_error("Failed to retrieve request URI!");
}

const std::string &Call::query_get(const std::string key)
{
	const auto q = query.find(key);

	if(q != query.cend())
		return q->second;
	throw std::runtime_error("Query argument not found!");
}

static size_t read_length(const int sock)
{
	char c;
	size_t length = 0;

	while(read(sock, &c, 1) > 0 && isdigit(c))
	{
		length = length * 10 + (c - '0');
		if(length > MAX_REQUEST_LENGTH)
			throw std::runtime_error("Request too long!");
	}
	return length;
}

static void read_request_(const int sock,
	char *buff, const size_t request_length)
{
	size_t i = 0;
	ssize_t len;

	while(i < request_length)
	{
		len = read(sock, buff + i, request_length - i);
		if(len < 0)
			throw std::runtime_error(strerror(errno));
		i += len;
	}
}

void Call::read_request()
{
	const auto request_length = read_length(sock);
	std::unique_ptr<char[]> ptr(new char[request_length]);
	char *request = ptr.get();

	read_request_(sock, request, request_length);
	parse_fields(request, request_length);
	parse_path();
	parse_body();
}

void Call::parse_fields(char *request, const size_t request_length)
{
	size_t i = 0;

	// TODO Might segfault if \0 is not present (bad request)
	while(i < request_length)
	{
		const std::string key(request + i);
		i += key.size() + 1;

		if(i > request_length)
			throw std::runtime_error("Bad request!");

		const std::string value(request + i);
		i += value.size() + 1;

		request_fields.emplace(key, value);
	}
}

void Call::parse_path()
{
	const auto path = get_request_field("DOCUMENT_URI");
	if(!path)
		throw std::runtime_error("Failed to retrieve request path!");
	this->path = *path;

	std::stringstream ss(*path);
	std::string buffer;

	while(std::getline(ss, buffer, '/'))
	{
		if(buffer.empty())
			continue;
		split_path.push_back(buffer);
	}

	const auto query = get_request_field("QUERY_STRING");
	if(!query)
		return;

	ss = std::stringstream(*query);
	size_t pos;

	while(std::getline(ss, buffer, '&'))
	{
		if(buffer.empty())
			continue;
		if((pos = buffer.find('=')) != std::string::npos)
		{
			this->query.emplace(std::string(buffer.cbegin(),
				buffer.cbegin() + pos),
					std::string(buffer.cbegin() + pos + 1,
						buffer.cend()));
		}
		else
			this->query.emplace(buffer, std::string());
	}
}

void Call::parse_body()
{
	const auto *content_length = get_request_field("CONTENT_LENGTH");
	size_t len, i = 0;

	if(!content_length)
		return;
	if((len = atol(content_length->c_str())) <= 0)
		return;
	// TODO Limit on len
	request_body.resize(len);
	while(i < len)
		i += read(sock, &request_body[i], len - i);
}

void Call::respond()
{
	if(done)
		return;
	response_fields["Status"] = std::to_string(response_status)
		+ ' ' + get_status(response_status);
	response_fields["Content-Length"] = std::to_string(response_body.size());

	// TODO Do only one allocation
	std::string response;

	for(const auto& f : response_fields)
		response += f.first + ": " + f.second + "\r\n";
	response += "\r\n";
	response += response_body;
	write(sock, response.data(), response.size());
	done = true;
	instance->log(std::string("Responded with status: ")
		+ std::to_string(response_status));
}
