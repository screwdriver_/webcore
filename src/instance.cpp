#include "web_core.hpp"
#include <thread>

using namespace Webcore;

static bool match(const char* str, const char* pattern)
{
	if(*pattern == '\0')
		return (*str == '\0');
	while(pattern[0] == '*' && pattern[1] == '*')
		++pattern;
	if(*str == '\0')
		return (*pattern == '*' && pattern[1] == '\0');
	if(*pattern == '*')
		return (match(str, pattern + 1) || match(str + 1, pattern));
	if(*pattern == '\\')
		++pattern;
	if(*str == *pattern)
		return match(str + 1, pattern + 1);
	return false;
}

static inline bool match(const std::string& str, const std::string& pattern)
{
	return match(str.c_str(), pattern.c_str());
}

Instance::Instance(const port_t port)
{
	sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);
	errno = 0;
	if(!(sock = socket(AF_INET, SOCK_STREAM, 0)) || errno)
		throw std::runtime_error(strerror(errno));
	errno = 0;
	if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
		throw std::runtime_error(strerror(errno));
	errno = 0;
	if(listen(sock, 16) < 0)
		throw std::runtime_error(strerror(errno));
}

Instance::~Instance()
{
	close(sock);
}

void Instance::log(const std::string &log)
{
	// TODO Date and time

	std::cout << log << std::endl;
	// TODO File
}

void Instance::accept_client()
{
	int client;
	sockaddr_in client_addr;
	socklen_t socklen = sizeof(client_addr);

	errno = 0;
	if((client = accept(sock, (sockaddr *) &client_addr, &socklen)) < 0)
		throw std::runtime_error(strerror(errno));
	try
	{
		handle_client(client);
	}
	catch(const std::exception &)
	{}
	close(sock);
}

void Instance::handle_client(const int client)
{
	Call call(this, client);

	try
	{
		handle(call);
	}
	catch(const std::exception &)
	{
		error(call, Webcore::INTERNAL_SERVER_ERROR);
	}
	if(!call.is_done())
		error(call, Webcore::NOT_FOUND);
}

void Instance::handle(Call &call)
{
	for(const auto& h : handlers)
	{
		if(call.is_done())
			break;
		if(call.response_status != OK)
		{
			error(call, call.response_status);
			break;
		}
		if(match(call.get_path(), h.first))
			h.second(call);
	}
}

const handler_t *Instance::get_error_handler(const http_status_t status)
{
	for(const auto &eh : error_handlers)
		if(eh.first == status)
			return &eh.second;
	return nullptr;
}

void Instance::error(Call &call, const http_status_t status)
{
	const auto handler = get_error_handler(status);

	if(handler)
		(*handler)(call);
}
