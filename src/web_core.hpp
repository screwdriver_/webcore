#ifndef WEBCORE_HPP
# define WEBCORE_HPP

# include <fstream>
# include <functional>
# include <iostream>
# include <memory>
# include <sstream>
# include <stdexcept>
# include <unordered_map>
# include <vector>

# include <errno.h>
# include <netinet/in.h>
# include <string.h>
# include <sys/socket.h>
# include <sys/types.h>
# include <unistd.h>

# define MAX_REQUEST_LENGTH	8388608
# define BUFFER_SIZE		1024

namespace Webcore
{
	class Call;
	class Instance;

	using http_status_t = uint16_t;
	using fields_t = std::unordered_map<std::string, std::string>;
	using port_t = uint16_t;
	using handler_t = std::function<void(Call &)>;
	using router_entry_t = std::pair<std::string, handler_t>;

	enum http_method : uint8_t
	{
		GET = 0,
		HEAD = 1,
		POST = 2,
		PUT = 3,
		DELETE = 4,
		TRACE = 5,
		OPTIONS = 6,
		CONNECT = 7,
		PATCH = 8
	};

	enum http_status : http_status_t
	{
		CONTINUE = 100,
		SWITCHING_PROTOCOLS = 101,
		PROCESSING = 102,

		OK = 200,
		CREATED = 201,
		ACCEPTED = 202,
		NON_AUTHORITATIVE_INFORMATION = 203,
		NO_CONTENT = 204,
		RESET_CONTENT = 205,
		PARTIAL_CONTENT = 206,
		MULTI_STATUS = 207,
		ALREADY_REPORTED = 208,
		IM_USED = 226,

		MULTIPLE_CHOICES = 300,
		MOVED_PERMANENTELY = 301,
		FOUND = 302,
		SEE_OTHER = 303,
		NOT_MODIFIED = 304,
		USE_PROXY = 305,
		SWITCH_PROXY = 306,
		TEMPORARY_REDIRECT = 307,
		PERMANENT_REDIRECT = 308,

		BAD_REQUEST = 400,
		UNAUTHORIZED = 401,
		PAYMENT_REQUIRED = 402,
		FORBIDDEN = 403,
		NOT_FOUND = 404,
		METHOD_NOT_ALLOWED = 405,
		NOT_ACCEPTABLE = 406,
		PROXY_AUTHENTICATION_REQUIRED = 407,
		REQUEST_TIMEOUT = 408,
		CONFLICT = 409,
		GONE = 410,
		LENGTH_REQUIRED = 411,
		PRECONDITION_FAILED = 412,
		PAYLOAD_TOO_LARGE = 413,
		URI_TOO_LONG = 414,
		UNSUPPORTED_MEDIA_TYPE = 415,
		RANGE_NOT_SATISFIABLE = 416,
		EXPECTATION_FAILED = 417,
		IM_A_TEAPOT = 418,
		MISDIRECTED_REQUEST = 421,
		UNPROCESSABLE_ENTITY = 422,
		LOCKED = 423,
		FAILED_DEPENDENCY = 424,
		UPGRADE_REQUIRED = 426,
		PRECONDITION_REQUIRED = 428,
		TOO_MANY_REQUESTS = 429,
		REQUEST_HEADER_FIELDS_TOO_LARGE = 431,
		UNAVAILABLE_FOR_LEGAL_REASONS = 451,

		INTERNAL_SERVER_ERROR = 500,
		NOT_IMPLEMENTED = 501,
		BAD_GATEWAY = 502,
		SERVICE_UNAVAILABLE = 503,
		GATEWAY_TIMEOUT = 504,
		HTTP_VERSION_NOT_SUPPORTED = 505,
		VARIANT_ALSO_NEGOTIATES = 506,
		INSUFFICIENT_STORAGE = 507,
		LOOP_DETECTED = 508,
		NOT_EXTENDED = 510,
		NETWORK_AUTHENTICATION_REQUIRED = 511
	};

	class Call
	{
		public:
		http_status_t response_status = OK;
		std::unordered_map<std::string, std::string> response_fields;
		std::string response_body;

		Call(Instance *instance, int sock);
		Call(const Call &) = delete;

		inline Instance &get_instance() const
		{
			return *instance;
		}

		const std::string &get_address() const;
		http_method get_request_method() const;
		const std::string &get_URI() const;

		inline const std::string &get_path() const
		{
			return path;
		}

		inline const std::vector<std::string> &get_split_path() const
		{
			return split_path;
		}

		inline bool query_has(const std::string key) const
		{
			return (query.find(key) != query.cend());
		}

		inline const std::unordered_map<std::string,
			std::string> get_query() const
		{
			return query;
		}

		const std::string &query_get(const std::string key);

		inline const fields_t& get_request_fields() const
		{
			return request_fields;
		}

		inline const std::string *get_request_field(const std::string key) const
		{
			const auto f = request_fields.find(key);
			return (f != request_fields.cend() ? &f->second : nullptr);
		}

		inline const std::string &get_request_body() const
		{
			return request_body;
		}

		inline bool is_done() const
		{
			return done;
		}

		void respond();

		private:
		Instance *instance;
		int sock;

		std::string path;
		std::unordered_map<std::string, std::string> query;

		fields_t request_fields;
		std::string request_body;

		std::vector<std::string> split_path;

		bool done = false;

		void read_request();
		void parse_fields(char *request, size_t request_length);
		void parse_path();
		void parse_body();
	};

	class Instance
	{
		public:
		Instance(port_t port);
		Instance(const Instance &) = delete;

		~Instance();

		void log(const std::string &log);

		inline void set_router(const std::initializer_list<router_entry_t> r)
		{
			handlers = std::vector<router_entry_t>(r);
		}

		inline void register_handler(const std::string &path, const handler_t h)
		{
			handlers.emplace_back(path, h);
		}

		inline void register_error_handler(http_status_t status,
			const handler_t h)
		{
			error_handlers.emplace_back(status, h);
		}

		void accept_client();

		private:
		int sock;

		std::vector<router_entry_t> handlers;
		std::vector<std::pair<http_status_t, handler_t>> error_handlers;

		void handle_client(int client_sock);
		void handle(Call &call);
		const handler_t *get_error_handler(http_status_t status);
		void error(Call &call, http_status_t status);
	};

	std::string read_file(const std::string &path);
	void set_tags(std::string &temp,
		const std::string &tag, const std::string &value);

	http_method get_method(const std::string &method);
	std::string get_status(http_status_t status);
}

#endif
