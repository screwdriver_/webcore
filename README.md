# WebCore

WebCore is a framework whose purpose is to provide a SCGI interface in order to create efficient websites in C++ language.

The framework's purpose is to accept incoming connections, parse an SCGI request coming from each connection and passing the data to a previously registered handler.
Then, the handler generates a response and the framework sends it back to the webserver.
